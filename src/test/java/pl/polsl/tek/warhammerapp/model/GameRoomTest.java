package pl.polsl.tek.warhammerapp.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.polsl.tek.warhammerapp.dto.AttackMessageDto;
import pl.polsl.tek.warhammerapp.dto.BattleLogMessageDto;
import pl.polsl.tek.warhammerapp.dto.CombatMessageDto;

import static org.junit.jupiter.api.Assertions.*;

class GameRoomTest {
    GameRoom gameroom;


    @BeforeEach
    void setUp() {
        gameroom = new GameRoom();
        gameroom.populateTestRoom();
    }
//        this.players.add(new Character("TestRoomSammy01","Sammy", "Clesvel",
//            new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
//            new Integer[] {20, 10},
//            "knife", "none"));
//        this.players.add(new Character("TestRoomTomasz01","Tomasz", "Noel",
//                                               new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
//            new Integer[] {10, 20},
//            "crossbow", "light"));
//        this.players.add(new Character("TestRoomMacer01","Macer", "Wari",
//                                               new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
//            new Integer[] {10, 10},
//            "sword", "heavy"));
//        this.enemies = new ArrayList<Character>();
//        this.enemies.add(new Character("TestRoomBeccaGoblinGrunt", "gamemaster", "Goblin Grunt",
//                                               new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
//            new Integer[] {15, 10},
//            "knife", "light"));
//        this.enemies.add(new Character("TestRoomBeccaGoblinArcher","gamemaster", "Goblin Archer",
//                                               new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
//            new Integer[] {15, 10},
//            "sword", "none"));


    @Test
    void addToBattlelog() {
        BattleLogMessageDto battleLogMessageDto = new BattleLogMessageDto("Test message","Player1", "player");
        String answer = gameroom.addToBattlelog(battleLogMessageDto);
        String expectedAnswer = "<b>Player1 (player): </b>Test message";
        assertEquals(expectedAnswer, answer);
    }

    @Test
    void attack() {
        AttackMessageDto attackMessageDto = new AttackMessageDto("player", "Clesvel", 0, "Goblin Grunt", 0 );
        CombatMessageDto combatMessageDto = gameroom.attack(attackMessageDto);
        assertEquals("Clesvel", combatMessageDto.getAttackerCharName());
        assertEquals("Goblin Grunt", combatMessageDto.getDefenderCharName());
    }

    @Test
    void locationHit() {
        String answer1 = gameroom.locationHit(90);
        assertEquals("head", answer1);
        String answer2 = gameroom.locationHit(54);
        assertEquals("body", answer2);
        String answer3 = gameroom.locationHit(00);
        assertEquals("right leg", answer3);
    }

    @Test
    void criticalCheck() {
        boolean answer1 = gameroom.criticalCheck(11);
        assertTrue(answer1);
        boolean answer2 = gameroom.criticalCheck(21);
        assertFalse(answer2);
    }

    @Test
    void simpleTest() {
        String answer1 = gameroom.simpleTest("incorrect message");
        assertEquals("", answer1);
        String answer2 = gameroom.simpleTest("#roll Clesvel WS -10");
        assertEquals( "Clesvel has made WS test and", answer2.substring(0, 28));

    }

    @Test
    void findAttribute() {
        Integer answer1 = gameroom.findAttribute("ws");
        assertEquals(0, answer1);
        Integer answer2 = gameroom.findAttribute("innnitiative");
        assertEquals(10, answer2);
        Integer answer3 = gameroom.findAttribute("willpower");
        assertEquals(8, answer3);
    }

    @Test
    void isBetween() {
        boolean answer1 = gameroom.isBetween(10, 30, 50);
        assertFalse(answer1);
        boolean answer2 = gameroom.isBetween(30,30, 50);
        assertTrue(answer2);
    }

    @Test
    void addPlayer() {
        Character character = new Character("TestId10", "Tomakaz", "Gregori",
                new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {20, 10},
                "knife", "none");
        gameroom.addPlayer(character);
        assertEquals(character, gameroom.getPlayer("Gregori"), "Player added to list");
    }

    @Test
    void removePlayer() {
        gameroom.removePlayer("Tomasz");
        assertNull(gameroom.getPlayer("Noel"));
    }

    @Test
    void switchOrAddPlayer() {
        Character character = new Character("TestId10", "Sammy", "Clesvel",
                new Integer[] {30, 80, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {20, 10},
                "knife", "none");
        gameroom.switchOrAddPlayer(character);
        assertEquals(80, gameroom.getPlayer("Clesvel").getCharAttr(1));
    }

    @Test
    void addEnemy() {
        Character character = new Character("TestId11", "gamemaster", "Goblin Scout",
                new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {20, 10},
                "knife", "none");
        gameroom.addEnemy(character);
        assertEquals(character, gameroom.getEnemy("Goblin Scout"));
    }

    @Test
    void removeEnemy() {
        gameroom.removeEnemy("Goblin Grunt");
        assertNull(gameroom.getEnemy("Goblin Grunt"));
    }

    @Test
    void switchOrAddEnemy() {
        Character character = new Character("TestId11", "gamemaster", "Goblin Grunt",
                new Integer[] {30, 80, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {20, 10},
                "knife", "none");
        gameroom.switchOrAddEnemy(character);
        assertEquals(80, gameroom.getEnemy("Goblin Grunt").getCharAttr(1));
    }

    @Test
    void addConnectedPlayer() {
        gameroom.addConnectedPlayer("Tomakaz");
        assertTrue(gameroom.getConnectedPlayers().contains("Tomakaz"));
    }

    @Test
    void removeConnectedPlayer() {
        gameroom.removeConnectedPlayer("Sammy");
        assertFalse(gameroom.getConnectedPlayers().contains("Sammy"));
    }
}