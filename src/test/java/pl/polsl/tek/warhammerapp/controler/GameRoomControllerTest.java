package pl.polsl.tek.warhammerapp.controler;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GameRoomControllerTest {

    // GET -------------------------------------------------------------------------------------------------------------
    @Test
    @Order(1)
    void getRooms() throws IOException {
        HttpGet request = new HttpGet("http://localhost:8080" + "/roomList");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        assertThat("GET response contains '[\"Test Room\",\"Orphan Room\"]'",
                responseContent, containsString("[\"Test Room\",\"Orphan Room\"]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    void getPresetItems() throws IOException {
        HttpGet request = new HttpGet("http://localhost:8080" + "/presetItems");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    void getPlayers() throws IOException {
        HttpGet request = new HttpGet("http://localhost:8080" + "/playerList?name=Test+Room");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        System.out.println(responseContent);
        assertThat("GET response contains '[\"Sammy\",\"Tomasz\",\"Macer\"]'",
                responseContent, containsString("[\"Sammy\",\"Tomasz\",\"Macer\"]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    void getRoom() throws IOException {
        HttpGet request = new HttpGet("http://localhost:8080" + "/room?name=Test+Room");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    void getHasGameMaster() throws IOException {
        HttpGet request = new HttpGet("http://localhost:8080" + "/hasGameMaster?name=Test+Room");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        assertThat("GET response contains true",
                responseContent, containsString("true"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    // POST ------------------------------------------------------------------------------------------------------------
    @Test
    @Order(6)
    void newGameRoom() throws IOException {
        String createJson = "{ \"id\": \"r1\", \"name\": \"Lichmaster\", \"gamemaster\": \"Fool357\", \"hasGameMaster\": \"true\", \"players\": [], \"enemies\": [], \"battleLog\": [\"Magia\"]}";

        HttpPost request = new HttpPost("http://localhost:8080"+ "/newRoom");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        request.setEntity(new StringEntity(createJson));
        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(201, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    void setNewGamemaster() throws IOException {
        HttpPost request = new HttpPost("http://localhost:8080"+ "/newGamemaster?roomName=Lichmaster&gamemasterName=Tomakaz");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(201, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(8)
    void newConnectedPlayerInRoom() throws IOException {
        HttpPost request = new HttpPost("http://localhost:8080"+ "/newConnectedPlayer?roomName=Lichmaster&playerName=Bambi");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(201, response.getStatusLine().getStatusCode());
    }

    // DELETE ----------------------------------------------------------------------------------------------------------


    @Test
    @Order(9)
    void removePlayer() throws IOException {
        HttpDelete request = new HttpDelete("http://localhost:8080" + "/removeConnectedPlayer?roomName=Lichmaster&playerName=Bambi");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(10)
    void deleteGamemasterStatus() throws IOException {
        HttpDelete request = new HttpDelete("http://localhost:8080" + "/removeGamemaster?roomName=Lichmaster");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(11)
    void closeRoom() throws IOException {
        HttpDelete request = new HttpDelete("http://localhost:8080" + "/closeRoom?name=Lichmaster");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}