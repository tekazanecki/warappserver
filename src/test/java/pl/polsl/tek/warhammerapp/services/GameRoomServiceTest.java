package pl.polsl.tek.warhammerapp.services;

import org.junit.jupiter.api.*;
import pl.polsl.tek.warhammerapp.model.GameRoom;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GameRoomServiceTest {
    GameRoomService gameRoomService = new GameRoomService();
    GameRoom additionalRoom;

    @BeforeEach
    void setRoom(){
        additionalRoom = new GameRoom();
        additionalRoom.setName("Additional Room");
    }

    @Test
    @Order(1)
    void addRoom() {
        gameRoomService.addRoom(additionalRoom);
        assertTrue(gameRoomService.getAllOfGameRooms().contains(additionalRoom));
        assertTrue(gameRoomService.getRoomList().contains("Additional Room"));
    }


    @Test
    @Order(2)
    void removeRoom() {
        gameRoomService.addRoom(additionalRoom);
        gameRoomService.removeRoom("Additional Room");
        assertFalse(gameRoomService.getAllOfGameRooms().contains(additionalRoom));
        assertFalse(gameRoomService.getRoomList().contains("Additional Room"));

    }
}