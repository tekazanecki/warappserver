package pl.polsl.tek.warhammerapp.model.presets;

import java.util.ArrayList;
import java.util.List;

public class WeaponList {

        private List<Weapon> weaponList;

        public WeaponList() {
            this.weaponList = new ArrayList<Weapon>();
            weaponList.add(new Weapon("unarmed", "UNR", true, true, 0));
            weaponList.add(new Weapon("knife", "KNF", true, true, 1));
            weaponList.add(new Weapon("sword", "SWD", true, true, 4));
            weaponList.add(new Weapon("crossbow", "CBW", false, false, 8));
        }

        public Weapon getWeapon(String name){
            for(Weapon weapon: weaponList){
                if(weapon.getName().equals(name)){
                    return weapon;
                }
            }
            return null;
        }

        public List<Weapon> getWeaponList() {
            return weaponList;
        }

        public void setWeaponList(List<Armor> armorList) {
            this.weaponList = weaponList;
        }
    }

