package pl.polsl.tek.warhammerapp.model.presets;

public class Weapon {

    private String name;
    private String code;
    private boolean isMelee;
    private boolean addStrength;
    private int damage;

    public Weapon() {
    }

    public Weapon(String name, String code, boolean isMelee, boolean addStrength, int damage) {
        this.name = name;
        this.code = code;
        this.isMelee = isMelee;
        this.addStrength = addStrength;
        this.damage = damage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsMelee() {
        return isMelee;
    }

    public void setMelee(Boolean melee) {
        isMelee = melee;
    }

    public Boolean getAddStrength() {
        return addStrength;
    }

    public void setAddStrength(Boolean addStrength) {
        this.addStrength = addStrength;
    }

    public Integer getDamage() {
        return damage;
    }

    public void setDamage(Integer damage) {
        this.damage = damage;
    }
}
