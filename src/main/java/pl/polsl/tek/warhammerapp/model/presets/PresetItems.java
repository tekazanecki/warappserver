package pl.polsl.tek.warhammerapp.model.presets;

public class PresetItems {

    private static PresetItems instance;

    private WeaponList weaponList;
    private ArmorList armorList;
    private StatusList statusList;

    private PresetItems() {
        this.weaponList = new WeaponList();
        this.armorList = new ArmorList();
        this.statusList = new StatusList();
    }

    public static PresetItems getInstance(){
        if(instance == null){
            instance = new PresetItems();
        }
        return instance;
    }

    public WeaponList getWeaponList() {
        return weaponList;
    }

    public void setWeaponList(WeaponList weaponList) {
        this.weaponList = weaponList;
    }

    public ArmorList getArmorList() {
        return armorList;
    }

    public void setArmorList(ArmorList armorList) {
        this.armorList = armorList;
    }

    public StatusList getStatusList() {
        return statusList;
    }

    public void setStatusList(StatusList statusList) {
        this.statusList = statusList;
    }
}
