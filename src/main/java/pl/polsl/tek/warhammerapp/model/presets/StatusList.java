package pl.polsl.tek.warhammerapp.model.presets;

import java.util.ArrayList;
import java.util.List;

public class StatusList {

    private List<Status> statusList;

    public StatusList() {
        this.statusList = new ArrayList<Status>();
        statusList.add(new Status("normal", "NORM"));
        statusList.add(new Status("unconscious", "UNKN"));
        statusList.add(new Status("dead", "DEAD"));

    }


    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }
}
