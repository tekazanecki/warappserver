package pl.polsl.tek.warhammerapp.model;

public class CriticalHit {

    private String description;
    private String location;
    private Integer wounds;
    private String effect;

    public CriticalHit() {
    }

    public CriticalHit(String description, String location,Integer wounds, String effect) {
        this.description = description;
        this.location = location;
        this.wounds = wounds;
        this.effect = effect;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWounds() {
        return wounds;
    }

    public void setWounds(Integer wounds) {
        this.wounds = wounds;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }
}

// ---------------------------------- clean template -------------------------------------------------------------------
//if(isBetween(secondRollForCritical, 1, 10)) return new CriticalHit("", 1, "");
//else if(isBetween(secondRollForCritical, 11, 20)) return new CriticalHit("", 1, "");
//else if(isBetween(secondRollForCritical, 21, 25)) return new CriticalHit("", 1, "");
//else if(isBetween(secondRollForCritical, 26, 30)) return new CriticalHit("", 1, "");
//else if(isBetween(secondRollForCritical, 31, 35)) return new CriticalHit("", 2, "");
//else if(isBetween(secondRollForCritical, 36, 40)) return new CriticalHit("", 2, "");
//else if(isBetween(secondRollForCritical, 41, 45)) return new CriticalHit("", 2, "");
//else if(isBetween(secondRollForCritical, 46, 50)) return new CriticalHit("", 2, "");
//else if(isBetween(secondRollForCritical, 51, 55)) return new CriticalHit("", 3, "");
//else if(isBetween(secondRollForCritical, 56, 60)) return new CriticalHit("", 3, "");
//else if(isBetween(secondRollForCritical, 61, 65)) return new CriticalHit("", 3, "");
//else if(isBetween(secondRollForCritical, 66, 70)) return new CriticalHit("", 3, "");
//else if(isBetween(secondRollForCritical, 71, 75)) return new CriticalHit("", 4, "");
//else if(isBetween(secondRollForCritical, 76, 80)) return new CriticalHit("", 4, "");
//else if(isBetween(secondRollForCritical, 81, 85)) return new CriticalHit("", 4, "");
//else if(isBetween(secondRollForCritical, 86, 90)) return new CriticalHit("", 4, "");
//else if(isBetween(secondRollForCritical, 91, 93)) return new CriticalHit("", 5, "");
//else if(isBetween(secondRollForCritical, 94, 96)) return new CriticalHit("", 5, "");
//else if(isBetween(secondRollForCritical, 97, 99)) return new CriticalHit("", 5, "");
//else if(isBetween(secondRollForCritical, 00, 00)) return new CriticalHit("", 99, "");
//else return new CriticalHit("error", 0 ,"error in critical location");