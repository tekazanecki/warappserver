package pl.polsl.tek.warhammerapp.model;

import java.util.ArrayList;
import java.util.List;

public class Character {

    private String userID;
    private String user;
    private String charName;
    // 0-WeaponSkill, 1-BallisticSkill, 2-Strength, 3-Toughness, 4-Initiative,
    // 5-Agility, 6-Dexterity, 7-Intelligence, 8-Willpower, 9-Fellowship
    private Integer [] charAttr;
    // 0-Melee, 1-Ranged
    private Integer [] charSkill;
    private Integer wounds;
    private Integer currentHealth;
    private String weapon;
    private String armor;
    private List<CriticalHit> criticalWounds;
    private String status;

    public Character() {
    }

    public Character(String userID, String user, String charName, Integer [] charAttr, Integer[] charSkill, String weapon, String armor) {
        this.userID = userID;
        this.user = user;
        this.charName = charName;
        this.charAttr = charAttr;
        this.charSkill = charSkill;
        this.wounds = charAttr[2]/10 + charAttr[3]/5 + charAttr[8]/10;
        this.currentHealth = this.wounds;
        this.weapon = weapon;
        this.armor = armor;
        this.criticalWounds = new ArrayList<>();
        this.status = "normal";
    }

    public Integer getMelee(){
        return charAttr[0] + charSkill[0];
    }

    public Integer getRanged(){
        return charAttr[1] + charSkill[1];
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public Integer[] getCharAttr() {
        return charAttr;
    }

    public Integer getCharAttr(Integer val) {
        return charAttr[val];
    }

    public void setCharAttr(Integer[] charAttr) {
        this.charAttr = charAttr;
    }

    public Integer[] getCharSkill() {
        return charSkill;
    }

    public void setCharSkill(Integer[] charSkill) {
        this.charSkill = charSkill;
    }

    public Integer getWounds() {
        return wounds;
    }

    public void setWounds(Integer wounds) {
        this.wounds = wounds;
    }

    public void receiveDamage(Integer damage) {
        if(this.currentHealth - damage <= 0){
            this.currentHealth = 0;
        } else {
            this.currentHealth -= damage;
        }
    }

    public Integer getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(Integer currentHealth) {
        this.currentHealth = currentHealth;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public List<CriticalHit> getCriticalWounds() {
        return criticalWounds;
    }

    public void setCriticalWounds(List<CriticalHit> criticalWounds) {
        this.criticalWounds = criticalWounds;
    }

    public void addCriticalWounds(List<CriticalHit> criticalWounds){
        for(CriticalHit critic : criticalWounds){
            this.criticalWounds.add(critic);
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
