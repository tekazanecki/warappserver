package pl.polsl.tek.warhammerapp.model.presets;

import java.util.ArrayList;
import java.util.List;

public class ArmorList {

    private List<Armor> armorList;

    public ArmorList() {
        this.armorList = new ArrayList<Armor>();
        armorList.add(new Armor("none", "NON", 0));
        armorList.add(new Armor("light", "LGT", 1));
        armorList.add(new Armor("medium", "MDM", 2));
        armorList.add(new Armor("heavy", "HEV", 3));
    }

    public Armor getArmor(String name){
        for(Armor armor: armorList){
            if(armor.getName().equals(name)){
                return armor;
            }
        }
        return null;
    }

    public List<Armor> getArmorList() {
        return armorList;
    }

    public void setArmorList(List<Armor> armorList) {
        this.armorList = armorList;
    }
}
