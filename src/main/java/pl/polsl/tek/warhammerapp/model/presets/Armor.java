package pl.polsl.tek.warhammerapp.model.presets;

public class Armor {

    private String name;
    private String code;
    private Integer value;

    public Armor() {
    }

    public Armor(String name, String code, Integer value) {
        this.name = name;
        this.code = code;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
