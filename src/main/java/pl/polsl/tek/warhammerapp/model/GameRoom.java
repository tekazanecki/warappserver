package pl.polsl.tek.warhammerapp.model;

import org.springframework.util.StringUtils;
import pl.polsl.tek.warhammerapp.dto.AttackMessageDto;
import pl.polsl.tek.warhammerapp.dto.BattleLogMessageDto;
import pl.polsl.tek.warhammerapp.dto.CombatMessageDto;
import pl.polsl.tek.warhammerapp.model.presets.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameRoom {

    private String id;
    private String name;
    private String gameMaster;
    private boolean hasGameMaster;
    private List<Character> players;
    private List<Character> enemies;
    private List<String> battleLog;
    private List<String> connectedPlayers;

    // --------------------------------- CONSTRUCTORS ------------------------------------------------------------------
    public GameRoom() {
        this.hasGameMaster = false;
        this.players = new ArrayList<>();
        this.enemies = new ArrayList<>();
        this.battleLog = new ArrayList<>();
        this.connectedPlayers = new ArrayList<>();

    }


    public GameRoom(String id, String name, String gameMaster) {
        this.id = id;
        this.name = name;
        this.gameMaster = gameMaster;
        this.hasGameMaster = true;
        this.players = new ArrayList<>();
        this.enemies = new ArrayList<>();
        this.battleLog = new ArrayList<>();
        this.connectedPlayers = new ArrayList<>();
        for(Character player: players){
            connectedPlayers.add(player.getUser());
        }
    }
    // --------------------------------- TEST LOGIC --------------------------------------------------------------------
    public void populateTestRoom() {
        this.id = "001";
        this.name = "Test Room";
        this.gameMaster = "Becca";
        this.hasGameMaster = true;
        this.players = new ArrayList<Character>();
        this.players.add(new Character("TestRoomSammy01","Sammy", "Clesvel",
                        new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                        new Integer[] {20, 10},
                        "sword", "none"));
        this.players.add(new Character("TestRoomTomasz01","Tomasz", "Noel",
                        new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                        new Integer[] {10, 20},
                        "crossbow", "light"));
        this.players.add(new Character("TestRoomMacer01","Macer", "Wari",
                        new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                        new Integer[] {10, 10},
                        "sword", "heavy"));
        this.enemies = new ArrayList<Character>();
        this.enemies.add(new Character("TestRoomBeccaGoblinGrunt", "gamemaster", "Goblin Grunt",
                        new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                        new Integer[] {15, 10},
                        "knife", "light"));
        this.enemies.add(new Character("TestRoomBeccaGoblinArcher","gamemaster", "Goblin Archer",
                        new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                        new Integer[] {15, 10},
                        "sword", "none"));
        this.battleLog = new ArrayList<String>();
        this.battleLog.add("To jest test");
        this.connectedPlayers = new ArrayList<String>();
        for(Character player: players){
            this.connectedPlayers.add(player.getUser());
        }

    }

    public void populateOrphanRoom() {
        this.id = "002";
        this.name = "Orphan Room";
        this.gameMaster = "";
        this.hasGameMaster = false;
        players = new ArrayList<Character>();
        players.add(new Character("TestRoomDMOAD01","DMOAD", "Arvius",
                new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {20, 10},
                "sword", "none"));
        players.add(new Character("TestRoomKris01","Kris", "Nekt",
                new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {10, 20},
                "crossbow", "light"));
        players.add(new Character("TestRoomRazi01","Razi", "Gerde",
                new Integer[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 30},
                new Integer[] {10, 10},
                "sword", "heavy"));
        enemies = new ArrayList<Character>();
        enemies.add(new Character("TestRoomFool357ChaosTroll","gamemaster", "Chaos Troll",
                new Integer[] {50, 30, 50, 50, 30, 30, 30, 30, 30, 30},
                new Integer[] {15, 10},
                "sword", "none"));
        battleLog = new ArrayList<String>();
        battleLog.add("To jest sierocy pokój");
        this.connectedPlayers = new ArrayList<String>();
        for(Character player: players){
            this.connectedPlayers.add(player.getUser());
        }
    }
    // --------------------------------- BUISNESS LOGIC ----------------------------------------------------------------

    public String addToBattlelog(BattleLogMessageDto battleLogMessageDto){
            String answer = "<b>" + battleLogMessageDto.getSender() + " (" + battleLogMessageDto.getStatus()+ "): </b>" + battleLogMessageDto.getMessage();
            this.battleLog.add(answer);
            return answer;
    }

    public CombatMessageDto attack(AttackMessageDto attackMessageDto){
        Random rand = new Random();
        boolean attackerIsPlayer = false;
        CriticalHitGenerator criticalHitGenerator = new CriticalHitGenerator();
        Character attacker, defender;
        Weapon attackerWeapon;
        Armor defenderArmor;
        int rollForAttacker = 0;
        int rollForDefender = 0;
        int attackerDiff = 0;
        int defenderDiff = 0;
        int attackerSL = 0;
        int defenderSL = 0;
        boolean attackerResult;
        boolean defenderResult;
        // answer
        Integer woundsForDefender = 0;
        Integer woundsForAttacker = 0;
        List<CriticalHit> attackCritical = new ArrayList<>();
        List<CriticalHit> counterattackCritical = new ArrayList<>();
        String finalMessage = "<b>Attack:</b> ";



        if(attackMessageDto.getInitializer().equals("player")){
            attacker = getPlayer(attackMessageDto.getAttackerName());
            defender = getEnemy(attackMessageDto.getDefenderName());
        } else{ //attackMessageDto.getInitializer() == "enemy"
            for(Character character: players){
                if(character.getCharName().equals(attackMessageDto.getAttackerName())){
                    attackerIsPlayer = true;
                }
            }
            if(attackerIsPlayer){
                attacker = getPlayer(attackMessageDto.getAttackerName());
                defender = getEnemy(attackMessageDto.getDefenderName());
            } else {
                attacker = getEnemy(attackMessageDto.getAttackerName());
                defender = getPlayer(attackMessageDto.getDefenderName());
            }

        }


        attackerWeapon = PresetItems.getInstance().getWeaponList().getWeapon(attacker.getWeapon());
        defenderArmor = PresetItems.getInstance().getArmorList().getArmor(defender.getArmor());

        // rollForHit --------------------------------------------------------------------------------------------------
        if(attackerWeapon.getIsMelee()){ //melee combat
            do {
                rollForAttacker = (rand.nextInt(100) + 1);
                rollForDefender = (rand.nextInt(100) + 1) ;
                attackerDiff = attacker.getMelee() + attackMessageDto.getAttackerMod();
                defenderDiff = defender.getMelee() + attackMessageDto.getDefenderMod();
                if (attackerDiff < 1) { attackerDiff = 1; }
                else if (attackerDiff > 100) { attackerDiff = 100; }
                if (defenderDiff < 1) { defenderDiff = 1; }
                else if (defenderDiff > 100) { defenderDiff = 100; }

                // ------------------------------------------- WHO WON -------------------------------------------------
                attackerSL = (int) (Math.floor(attackerDiff / 10) - Math.floor(rollForAttacker / 10));
                defenderSL = (int) (Math.floor(defenderDiff / 10) - Math.floor(rollForDefender / 10));
                attackerResult = rollForAttacker <= attackerDiff;
                defenderResult = rollForDefender <= defenderDiff;
            }while(attackerSL == defenderSL);

            // ---------------------------------------------- CRITICS AND FUMBLES --------------------------------------

            // ATTACKER
            if(criticalCheck(rollForAttacker)&&(attackerResult)) { // attacker critical hit
                CriticalHit tmp = criticalHitGenerator.criticalHitRoll();
                attackCritical.add(tmp);
                woundsForDefender += tmp.getWounds();
                finalMessage += attackMessageDto.getAttackerName() + " score a Critical Hit: <i>" + tmp.getDescription() + "</i>.<br>";
            } else if(criticalCheck(rollForAttacker)&&(!attackerResult)) { // attacker fumble
                String fumbleText;
                fumbleText = fumble();
                if (fumbleText.substring(0, 6).equals("You ca")) {
                    woundsForAttacker += 1;
                } else if (fumbleText.substring(0, 6).equals("You ov")) {
                    counterattackCritical.add(new CriticalHit("Torn Muscles", "GM decision", 0, "One of your muscles is sprained or torn, resulting in impaired capabilities and much pain. Suffer a penalty of –10 to all Tests involving the location. If a Leg is hit, also halve your Movement."));
                }
                finalMessage += attackMessageDto.getAttackerName() + " as part of his attack get fumble: <i>" + fumbleText + "</i>.<br>";
            }
            // DEFENDER
            else if(!defender.getStatus().equals("dead") && !defender.getStatus().equals("unconscious")) {
                 if (criticalCheck(rollForDefender) && (defenderResult)) { // defender critical hit
                    CriticalHit tmp = criticalHitGenerator.criticalHitRoll();
                    counterattackCritical.add(tmp);
                    woundsForAttacker += tmp.getWounds();
                    finalMessage += attackMessageDto.getDefenderName() + " score a Critical Hit in counterattack: <i>" + tmp.getDescription() + "</i>.<br>";
                } else if (criticalCheck(rollForDefender) && (!defenderResult)) { // defender fumble
                    String fumbleText = fumble();
                    if (fumbleText.substring(0, 6).equals("You ca")) {
                        woundsForDefender += 1;
                    } else if (fumbleText.substring(0, 6).equals("You ov")) {
                        attackCritical.add(new CriticalHit("Torn Muscles", "GM decision", 0, "One of your muscles is sprained or torn, resulting in impaired capabilities and much pain. Suffer a penalty of –10 to all Tests involving the location. If a Leg is hit, also halve your Movement."));
                    }
                    finalMessage += attackMessageDto.getDefenderName() + " get fumble: <i>" + fumbleText + "</i>.<br>";
                }
            }

        }
        else { // ranged combat
            rollForAttacker = (rand.nextInt(100) + 1);
            attackerDiff = attacker.getRanged() + attackMessageDto.getAttackerMod();
            if (attackerDiff < 1) { attackerDiff = 1; }
            else if (attackerDiff > 100) { attackerDiff = 100; }

            // ------------------------------------------- WHO WON -------------------------------------------------
            attackerSL = attackerDiff / 10 - rollForAttacker / 10;
            attackerResult = rollForAttacker <= attackerDiff;

            // ---------------------------------------------- CRITICS AND FUMBLES --------------------------------------

            // ATTACKER
            if(criticalCheck(rollForAttacker)&&(attackerResult)) { // attacker critical hit
                CriticalHit tmp = criticalHitGenerator.criticalHitRoll();
                attackCritical.add(tmp);
                woundsForDefender += tmp.getWounds();
                finalMessage += attackMessageDto.getAttackerName() + " score a Critical Hit: <i>" + tmp.getDescription() + "</i>.<br>";
            } else if(criticalCheck(rollForAttacker)&&(!attackerResult)) { // attacker fumble
                String fumbleText = fumble();
                if (fumbleText.substring(0, 6).equals("You ca")) {
                    woundsForAttacker += 1;
                } else if (fumbleText.substring(0, 6).equals("You ov")) {
                    counterattackCritical.add(new CriticalHit("Torn Muscles", "GM decision", 0, "One of your muscles is sprained or torn, resulting in impaired capabilities and much pain. Suffer a penalty of –10 to all Tests involving the location. If a Leg is hit, also halve your Movement."));
                }
                finalMessage += attackMessageDto.getAttackerName() + " as part of his attack get fumble: <i>" + fumbleText + "</i>.<br>";
            }

        }

        Boolean hit;
        if(attackerWeapon.getIsMelee()){ hit = attackerSL > defenderSL; }
        else { hit = attackerResult; }
        // ---------------------------------------------- ACTUAL HIT -----------------------------------------------
        if(hit){ //attack hit
            Integer hitDamage;
            Integer bonusFromSL = attackerSL - defenderSL;
            if(attackerWeapon.getIsMelee()){
                hitDamage = 1 + attackerWeapon.getDamage() + bonusFromSL +  (attacker.getCharAttr(2)/10) - defenderArmor.getValue() - (defender.getCharAttr(3)/10);
                System.out.println(hitDamage);
            } else {
                hitDamage = 1 + attackerWeapon.getDamage() + bonusFromSL - defenderArmor.getValue() - (defender.getCharAttr(3)/10);
            }
            if(hitDamage < 0){ hitDamage = 0; }
            woundsForDefender += hitDamage;
            finalMessage += attackMessageDto.getAttackerName() + " hit " + attackMessageDto.getDefenderName() + " in " + locationHit(rollForAttacker) + " for " + hitDamage + " damage. Attacker roll " + rollForAttacker + "/" + attackerDiff + "(" + attackerSL + ")";
            if(attackerWeapon.getIsMelee()){
                finalMessage += " vs deffender roll " + rollForDefender + "/" + defenderDiff + "(" + defenderSL + ").";
            }
        } else {
            finalMessage +=  attackMessageDto.getAttackerName() + " miss his attack roll " + rollForAttacker + "/" + attackerDiff + "(" + attackerSL + ")";
            if(attackerWeapon.getIsMelee()){
                finalMessage += " vs deffender roll " + rollForDefender + "/" + defenderDiff + "(" + defenderSL + ").";
            }

        }
        attacker.receiveDamage(woundsForAttacker);
        defender.receiveDamage(woundsForDefender);
        finalMessage = checkStatusOfCharacters(criticalHitGenerator, attacker, woundsForAttacker, counterattackCritical, finalMessage);
        finalMessage = checkStatusOfCharacters(criticalHitGenerator, defender, woundsForDefender, attackCritical, finalMessage);


        this.battleLog.add(finalMessage);

        return new CombatMessageDto(
                attackMessageDto.getAttackerName(),
                attacker.getStatus(),
                attacker.getCurrentHealth(),
                counterattackCritical,
                attackMessageDto.getDefenderName(),
                defender.getStatus(),
                defender.getCurrentHealth(),
                attackCritical,
                finalMessage

        );

    }

    private String checkStatusOfCharacters(CriticalHitGenerator criticalHitGenerator, Character combatant, Integer woundsForCombatant, List<CriticalHit> criticForOpponent, String finalMessage) {
        if(combatant.getCurrentHealth() == 0 && woundsForCombatant > 0 && !combatant.getStatus().equals("dead") && !combatant.getStatus().equals("unconscious")){
            combatant.setStatus("unconscious");
            CriticalHit tmp = criticalHitGenerator.criticalHitRoll();
            criticForOpponent.add(tmp);
            finalMessage += "<br>" + combatant.getCharName() + " lose consciousness and receive critical hit: " + tmp.getDescription();
            combatant.addCriticalWounds(criticForOpponent);
        } else if(combatant.getCurrentHealth() == 0 && woundsForCombatant > 0 && !combatant.getStatus().equals("dead") && combatant.getStatus().equals("unconscious")) {
            CriticalHit tmp = criticalHitGenerator.criticalHitRoll();
            criticForOpponent.add(tmp);
            finalMessage += "<br>" + combatant.getCharName() + " receive critical hit: " + tmp.getDescription();
        }
        if(combatant.getCriticalWounds().size() >= (combatant.getCharAttr(3))/10){
            combatant.setStatus("dead");
            finalMessage += "<br>" + combatant.getCharName() + " is dead";

        }
        return finalMessage;
    }

    public String locationHit(Integer score){
        Integer reversed = 0;

        if (score < 10) {
            reversed = score*10;
        } else {
            while(score != 0) {
                int digit = score % 10;
                reversed = (reversed * 10) + digit;
                score /= 10;
            }

        }
        if(isBetween(reversed, 1, 9)){ return "head";}
        else if(isBetween(reversed, 10, 24)){ return "left arm";}
        else if(isBetween(reversed, 25, 44)){ return "right arm";}
        else if(isBetween(reversed, 45, 79)){ return "body";}
        else if(isBetween(reversed, 80, 89)){ return "left leg";}
        else return "right leg";
    }



    public String fumble(){
        Random rand = new Random();
        Integer rollForFumble = rand.nextInt(100)+1;
        if(isBetween(rollForFumble, 1, 20)){ return "You catch a part of your anatomy (we recommend you play this for laughs) — lose 1 Wound, ignoring Toughness Bonus or Armour Points.";}
        else if(isBetween(rollForFumble, 21, 40)){ return "Your melee weapon jars badly, or ranged weapon malfunctions or slightly breaks – your weapon suffers 1 Damage. Next round, you will act last regardless of Initiative order, Talents, or special rules as you recover.";}
        else if(isBetween(rollForFumble, 41, 60)){ return "Your manoeuvre was misjudged, leaving you out of position, or you lose grip of a ranged weapon. Next round, your Action suffers a penalty of –10.";}
        else if(isBetween(rollForFumble, 61, 70)){ return "You stumble badly, finding it hard to right yourself. Lose your next Move.";}
        else if(isBetween(rollForFumble, 71, 80)){ return "You mishandle your weapon, or you drop your ammunition. Miss your next Action.";}
        else if(isBetween(rollForFumble, 81, 90)){ return "You overextend yourself or stumble and twist your ankle. Suffer a Torn Muscle (Minor) injury (see page 179). This counts as a Critical Wound.";}
        else return "You completely mess up, hitting 1 random ally in range using your rolled units die to determine the SL of the hit. If that’s not possible, you somehow hit yourself in the face and gain a Stunned Condition.";
    }



    public boolean criticalCheck(Integer score) {
        String tmp;
        if (score < 10) {
            tmp = '0' + score.toString();
        } else {
            tmp = score.toString();
        }
        if (tmp.charAt(0)==tmp.charAt(1)){
            return true;
        } else {
            return false;
        }
    }

    public String simpleTest(String message){
        // #roll Mike toughness -10
        Random rand = new Random();
        String tmp = message.toLowerCase();
        String[] words = tmp.split(" ");
        String charName = StringUtils.capitalize(words[1]);
        Integer nameLength = 0;
        if(charName.charAt(0) == '('){
            charName = message.substring(message.indexOf("(")+1);
            charName = charName.substring(0, charName.indexOf(")"));
            nameLength = charName.split(" ").length-1;
        }
        String testType;
        Integer testAttrNumber;
        Integer testMod;


        // --------------------- CHECK CHARACTER NAME ----------------------------------
        Character testCharacter = null;
        Boolean found = false;
        for(Character player: players){
            if(player.getCharName().equals(charName)){
                testCharacter = player;
                found = true;
            }
        }
        if(!found){
            for(Character enemy: enemies){

                if(enemy.getCharName().equals(charName)){
                      testCharacter = enemy;
                    found = true;
                }
            }
        }
        if(!found) { return ""; }
        // ------------------------ CHECK ATTRIBUTE NAME -------------------------------
        testType = words[2+nameLength];
        testAttrNumber = findAttribute(testType);

        if(testAttrNumber == 10){ return ""; }
        testType = testType.toUpperCase();

        // ------------------------ CHECK ATTRIBUTE MOD --------------------------------

        try{
            testMod = Integer.parseInt(words[3+nameLength]);
        }catch(NumberFormatException e){
            return "";
        }


        // ------------------------------- ROLL ----------------------------------------
        Integer diceRoll = rand.nextInt(100)+1;
        Integer diff = testCharacter.getCharAttr(testAttrNumber) + testMod;
        Integer SL = diff/10 - diceRoll/10;
        String finalEffect;
        if(diff>=diceRoll){ finalEffect = "SUCCEED"; }
        else { finalEffect = "FAILED"; }
        String answer = charName + " has made " + testType + " test and " + finalEffect + " with " + SL + "SL (difficulty: " + diff + " vs roll: "+ diceRoll + ")";
        this.battleLog.add(answer);
        return answer;
    }

    public Integer findAttribute(String charAttr){
        if(charAttr.equals("weaponskill") || charAttr.equals("ws")) { return 0;}
        else if(charAttr.equals("ballisticskill") || charAttr.equals("bs")) { return 1;}
        else if(charAttr.equals("strength") || charAttr.equals("s")) { return 2;}
        else if(charAttr.equals("toughness") || charAttr.equals("t")) { return 3;}
        else if(charAttr.equals("initiative") || charAttr.equals("i")) { return 4;}
        else if(charAttr.equals("agility") || charAttr.equals("ag")) { return 5;}
        else if(charAttr.equals("dexterity") || charAttr.equals("dex")) { return 6;}
        else if(charAttr.equals("intelligence") || charAttr.equals("int")) { return 7;}
        else if(charAttr.equals("willpower") || charAttr.equals("wp")) { return 8;}
        else if(charAttr.equals("fellowship") || charAttr.equals("fel")) { return 9;}
        else return 10;

    }


    public boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    // --------------------------------- GETTERS & SETTERS --------------------------------------------------------------
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGameMaster() {
        return gameMaster;
    }

    public void setGameMaster(String gameMaster) {
        this.gameMaster = gameMaster;
    }

    public Boolean getHasGameMaster() { return hasGameMaster; }

    public void setHasGameMaster(Boolean hasGameMaster) { this.hasGameMaster = hasGameMaster; }

    public void addPlayer(Character character){
        this.players.add(character);
    }

    public Character getPlayer(String playerName){
        for(Character character: players){
            if(character.getCharName().equals(playerName)){
                return character;
            }
        }
        return null;
    }

    public void removePlayer(String playerName){
        for(Character character: players){
            if(character.getUser().equals(playerName)){
                this.players.remove(character);
                break;
            } else {
                System.out.println("there was attempt to delete not existing character");
            }
        }
    }

    public void switchOrAddPlayer(Character newCharacter){
        Boolean found = false;
        for(Character character: players){
            if(character.getCharName().equals(newCharacter.getCharName())){
                int i = this.players.indexOf(character);
                this.players.set(i, newCharacter);
                found = true;
            }
        }
        if(!found){
            this.players.add(newCharacter);
        }
    }

    public List<Character> getPlayers() {
        return players;
    }

    public void setPlayers(List<Character> players) {
        this.players = players;
    }

    public void addEnemy(Character character){
        this.enemies.add(character);
    }

    public Character getEnemy(String enemyName){
        for(Character character: enemies){
            if(character.getCharName().equals(enemyName)){
                return character;
            }
        }
        return null;
    }

    public void removeEnemy(String enemyName){
        for(Character character: enemies){
            if(character.getCharName().equals(enemyName)){
                this.enemies.remove(character);
            }
        }
    }

    public void switchOrAddEnemy(Character newCharacter){
        Boolean found = false;
        for(Character character: enemies){
            if(character.getCharName().equals(newCharacter.getCharName())){
                int i = this.enemies.indexOf(character);
                this.enemies.set(i, newCharacter);
                found = true;
            }
        }
        if(!found){
            this.enemies.add(newCharacter);
        }
    }

    public List<Character> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Character> enemies) {
        this.enemies = enemies;
    }

    public List<String> getBattleLog() {
        return battleLog;
    }

    public void setBattleLog(List<String> battleLog) {
        this.battleLog = battleLog;
    }

    public List<String> getConnectedPlayers() {
        return connectedPlayers;
    }

    public void addConnectedPlayer(String name){
        this.connectedPlayers.add(name);
    }

    public void removeConnectedPlayer(String name){
        if(this.connectedPlayers.contains(name)){
            this.connectedPlayers.remove(name);
        } else {
            System.out.println("There was attempt to delete not existing connected player");
        }
    }

    public void setConnectedPlayers(List<String> connectedPlayers) {
        this.connectedPlayers = connectedPlayers;
    }
}
