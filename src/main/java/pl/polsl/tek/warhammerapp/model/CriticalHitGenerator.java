package pl.polsl.tek.warhammerapp.model;

import java.util.Random;

public class CriticalHitGenerator {

    public CriticalHitGenerator() {
    }

    public CriticalHit criticalHitRoll() {
        Random rand = new Random();
        Integer firstRollForLocation = rand.nextInt(100)+1;
        Integer secondRollForCritical = rand.nextInt(100)+1;


        if (isBetween(firstRollForLocation, 1, 9)) {
            // head ----------------------------------------------------------------------------------------------------
            if(isBetween(secondRollForCritical, 1, 10))       return new CriticalHit("Dramatic Ijury", "head", 1, "A fine wound across the forehead and cheek. Gain 1 Bleeding Condition. Once the wound is healed, the impressive scar it leaves provides a bonus of +1 SL to appropriate social Tests. You can only gain this benefit once.");
            else if(isBetween(secondRollForCritical, 11, 20)) return new CriticalHit("Minor Cut", "head", 1, "The strike opens your cheek and blood flies everywhere. Gain 1 Bleeding Condition. ");
            else if(isBetween(secondRollForCritical, 21, 25)) return new CriticalHit("Poked Eye", "head", 1, "The blow glances across your eye socket. Gain 1 Blinded condition");
            else if(isBetween(secondRollForCritical, 26, 30)) return new CriticalHit("Ear Bash", "head", 1, "Your ear takes a sickening impact, leaving it ringing. The Gain 1 Deafened Condition.");
            else if(isBetween(secondRollForCritical, 31, 35)) return new CriticalHit("Rattling Blow", "head", 2, "The blow floods your vision with spots and flashing lights. Gain 1 Stunned Condition.");
            else if(isBetween(secondRollForCritical, 36, 40)) return new CriticalHit("Black Eye", "head", 2, "A solid blow hits your eye, leaving tears and much pain. Gain 2 Blinded Conditions.");
            else if(isBetween(secondRollForCritical, 41, 45)) return new CriticalHit("Sliced Ear", "head", 2, "Your side of your head takes a hard blow, cutting deep into your ear. Gain 2 Deafened and 1 Bleeding Condition.");
            else if(isBetween(secondRollForCritical, 46, 50)) return new CriticalHit("Struck Forehead", "head", 2, "A solid blow thumps into the centre of your forehead. Gain 2 Bleeding Conditions and a Blinded Condition that cannot be removed until all Bleeding Conditions are removed. ");
            else if(isBetween(secondRollForCritical, 51, 55)) return new CriticalHit("Fractured Jaw", "head", 3, "With a sickening crunch, pain fills your face as the blow fractures your jaw. Gain 2 Stunned Conditions. Suffer a Broken Bone (Minor) injury.");
            else if(isBetween(secondRollForCritical, 56, 60)) return new CriticalHit("Major Eye Wound", "head", 3, "The blow cracks across your eye socket. Gain 1 Bleeding Condition. Also gain 1 Blinded Condition that cannot be removed until you receive Medical Attention.");
            else if(isBetween(secondRollForCritical, 61, 65)) return new CriticalHit("Major Ear Wound", "head", 3, "The blow damages your ear, leaving you with permanent hearing loss in one ear. Suffer a –20 penalty on all Tests relating to hearing. If you suffer this result again, your hearing is permanently lost as the second ear falls quiet. Only magic can heal this.");
            else if(isBetween(secondRollForCritical, 66, 70)) return new CriticalHit("Broken Nose", "head", 3, "A solid blow to the centre of your face causing blood to pour. Gain 2 Bleeding Conditions. Make a Challenging (+0) Endurance Test, or also gain a Stunned Condition. After this wound has healed, gain +1/–1 SL on social rolls, depending on context, unless Surgery is used to reset the nose. ");
            else if(isBetween(secondRollForCritical, 71, 75)) return new CriticalHit("Broken Jaw", "head", 4, "The crack is sickening as the blow hits you under the chin, breaking your jaw. Gain 3 Stunned Conditions. Make a Challenging (+0) Endurance Test or gain an Unconscious Condition. Suffer a Broken Bone (Major) injury.");
            else if(isBetween(secondRollForCritical, 76, 80)) return new CriticalHit("Concussive Blow", "head", 4, "Your brain rattles in your skull as blood spurts from your nose and ears. Take 1 Deafened , 2 Bleeding , and 1d10 Stunned Conditions. Gain a Fatigued Condition that lasts for 1d10 days. If you receive another Critical Wound to your head while suffering this Fatigued Condition, make an Average (+20) Endurance Test or also gain an Unconscious Condition.");
            else if(isBetween(secondRollForCritical, 81, 85)) return new CriticalHit("Smashed Mouth", "head", 4, "With a sickening crunch, your mouth is suddenly filled with broken teeth and blood. Gain 2 Bleeding Conditions. Lose 1d10 teeth — Amputation (Easy).");
            else if(isBetween(secondRollForCritical, 86, 90)) return new CriticalHit("Mangled Eye", "head", 4, "Little is left of your ear as the blow tears it apart. You gain 3 Deafened and 2 Bleeding Conditions. Lose your ear —Amputation (Average).");
            else if(isBetween(secondRollForCritical, 91, 93)) return new CriticalHit("Devastated Eye", "head", 5, "A strike to your eye completely bursts it, causing extraordinary pain. Gain 3 Blinded , 2 Bleeding , and 1 Stunned Condition. Lose your eye — Amputation (Difficult).");
            else if(isBetween(secondRollForCritical, 94, 96)) return new CriticalHit("Disfiguring Blow", "head", 5, "The blow smashes your entire face, destroying your eye and nose in a cloud of blood. Gain 3 Bleeding , 3 Blinded and 2 Stunned Conditions. Lose your eye and nose — Amputation (Hard).");
            else if(isBetween(secondRollForCritical, 97, 99)) return new CriticalHit("Mangled Jaw", "head", 5, "The blow almost removes your jaw as it utterly destroys your tongue, sending teeth flying in a shower of blood. Gain 4 Bleeding and 3 Stunned Conditions. Make a Very Hard (–30) Endurance Test or gain an Unconscious Condition. Suffer a Broken Bone (Major) injury and lose your tongue and 1d10 teeth — Amputation (Hard). ");
            else if(isBetween(secondRollForCritical, 00, 00)) return new CriticalHit("Decapitated",  "head",99, "Your head is entirely severed from your neck and soars through the air, landing 1d10 feet away in a random direction (see Scatter). Your body collapses, instantly dead.");
            else                                                          return new CriticalHit("error", "none", 0 ,"error in critical location");
        } else if (isBetween(firstRollForLocation, 10, 24)) {
            //left arm -------------------------------------------------------------------------------------------------
            return armCriticalWounds(secondRollForCritical, "left arm");
        } else if (isBetween(firstRollForLocation, 25, 44)) {
            //right arm ------------------------------------------------------------------------------------------------
            return armCriticalWounds(secondRollForCritical, "right arm");
        } else if (isBetween(firstRollForLocation, 45, 79)) {
            //body -----------------------------------------------------------------------------------------------------
            if(isBetween(secondRollForCritical, 1, 10))       return new CriticalHit("This But A Scratch!", "body", 1, "Gain 1 Bleeding Condition.");
            else if(isBetween(secondRollForCritical, 11, 20)) return new CriticalHit("Gut Blow", "body", 1, "Gain 1 Stunned Condition. Pass an Easy (+40) Endurance Test, or vomit, gaining the Prone Condition.");
            else if(isBetween(secondRollForCritical, 21, 25)) return new CriticalHit("Low Blow!", "body", 1, "Make a Hard (-20) Endurance Test or gain 3 Stunned Condition. ");
            else if(isBetween(secondRollForCritical, 26, 30)) return new CriticalHit("Twisted Back", "body", 1, "Suffer a Torn Muscle (Minor) injury.");
            else if(isBetween(secondRollForCritical, 31, 35)) return new CriticalHit("Winded", "body", 2, "Gain a Stunned Condition. Make an Average (+20) Endurance Test, or gain the Prone Condition. Movement is halved for 1d10 rounds as you get your breath back.");
            else if(isBetween(secondRollForCritical, 36, 40)) return new CriticalHit("Bruised Ribs", "body", 2, "All Agility-based Tests suffer a –10 penalty for 1d10 days.");
            else if(isBetween(secondRollForCritical, 41, 45)) return new CriticalHit("Wrenched Collar Bone", "body", 2, "Randomly select one arm. Drop whatever is held in that hand; the arm is useless for 1d10 rounds (see Amputated Parts).");
            else if(isBetween(secondRollForCritical, 46, 50)) return new CriticalHit("Ragged Wound", "body", 2, "Take 2 Bleeding Conditions.");
            else if(isBetween(secondRollForCritical, 51, 55)) return new CriticalHit("Cracked Ribs", "body", 3, "The hit cracks one or more ribs. Gain a Stunned Condition. Gain a Broken Bone (Minor) injury.");
            else if(isBetween(secondRollForCritical, 56, 60)) return new CriticalHit("Gaping Wound", "body", 3, "Take 3 Bleeding Conditions. Until you receive Surgery, any Wounds you receive to the Body Hit Location will inflict an additional Bleeding Condition as the cut reopens.");
            else if(isBetween(secondRollForCritical, 61, 65)) return new CriticalHit("Painful Cut", "body", 3, "Gain 2 Bleeding Conditions and a Stunned Condition. Take a Hard (–20) Endurance Test or gain the Unconscious Condition as you black out from the pain. Unless you achieve 4+ SL, you also scream out in agony.");
            else if(isBetween(secondRollForCritical, 66, 70)) return new CriticalHit("Arterial Damage", "body", 3, "Gain 4 Bleeding Conditions. Until you receive Surgery, every time you receive Damage to the Body Hit Location, gain 2 Bleeding Conditions.");
            else if(isBetween(secondRollForCritical, 71, 75)) return new CriticalHit("Pulled Back", "body", 4, "Your back turns to white pain as you pull a muscle. Suffer a Torn Muscle (Major) injury.");
            else if(isBetween(secondRollForCritical, 76, 80)) return new CriticalHit("Fractured Hip", "body", 4, "Gain a Stunned Condition. Take a Challenging (+0) Endurance Test or also gain the Prone Condition. Suffer a Broken Bone (Minor) injury.");
            else if(isBetween(secondRollForCritical, 81, 85)) return new CriticalHit("Major Chest Wound", "body", 4, "You take a significant wound to your chest, flensing skin from muscle and sinew. Take 4 Bleeding Conditions. Until you receive Surgery, to stitch the wound together, any Wounds you receive to the Body Hit Location will also inflict 2 Bleeding Conditions as the tears reopen.");
            else if(isBetween(secondRollForCritical, 86, 90)) return new CriticalHit("Gut Wound", "body", 4, "Contract a Festering Wound (see Disease and Infection) and gain 2 Bleeding Conditions.");
            else if(isBetween(secondRollForCritical, 91, 93)) return new CriticalHit("Smashed Rib Cage", "body", 5, "Gain a Stunned Condition that can only be removed through Medical Attention, and suffer a Broken Bone (Major) injury.");
            else if(isBetween(secondRollForCritical, 94, 96)) return new CriticalHit("Broken Collar Bone", "body", 5, "Gain the Unconscious Condition until you receive Medical Attention, and suffer a Broken Bone (Major) injury.");
            else if(isBetween(secondRollForCritical, 97, 99)) return new CriticalHit("Internal bleeding", "body", 5, "Gain a Bleeding Condition that can only be removed through Surgery. Contract Blood Rot (see Disease and Infection). ");
            else if(isBetween(secondRollForCritical, 00, 00)) return new CriticalHit("Torn Apart", "body", 99, "You are hacked in two. The top half lands in a random direction, and all characters within 2 yards are showered in blood.");
            else                                                          return new CriticalHit("error", "none", 0 ,"error in critical location");
        } else if (isBetween(firstRollForLocation, 80, 89)) {
            //left leg -------------------------------------------------------------------------------------------------
            return legCriticalWounds(secondRollForCritical, "left leg");
        } else if (isBetween(firstRollForLocation, 90, 100)) {
            //left arm -------------------------------------------------------------------------------------------------
            return legCriticalWounds(secondRollForCritical, "right leg");
        }
        return null;
    }
    public CriticalHit armCriticalWounds(Integer secondRoll, String location){
        if(isBetween(secondRoll, 1, 10))       return new CriticalHit("Jarred Arm", location, 1, "Your arm is jarred in the attack. Drop whatever was held in that hand.");
        else if(isBetween(secondRoll, 11, 20)) return new CriticalHit("Minor cut", location, 1, "Gain a Bleeding Condition as your upper arm is cut badly.");
        else if(isBetween(secondRoll, 21, 25)) return new CriticalHit("Sprain", location, 1, "You sprain your arm, suffering a Torn Muscle (Minor) injury.");
        else if(isBetween(secondRoll, 26, 30)) return new CriticalHit("Badly Jarred Arm", location, 2, "Your arm is badly jarred in the attack. Drop whatever was held in that hand, which is useless for 1d10 – Toughness Bonus Rounds (minimum 1). For this time, treat the hand as lost (see Amputated Parts). ");
        else if(isBetween(secondRoll, 31, 35)) return new CriticalHit("Torn Muscles", location, 2, "The blow slams into your forearm. Gain a Bleeding Condition and a Torn Muscle (Minor) injury.");
        else if(isBetween(secondRoll, 36, 40)) return new CriticalHit("Bleeding Hand", location, 2, "Your hand is cut badly, making your grip slippery. Take 1 Bleeding Condition. While suffering from that Bleeding Condition, make an Average (+20) Dexterity Test before taking any Action that requires something being held in that hand; if you fail, the item slips from your grip. ");
        else if(isBetween(secondRoll, 41, 45)) return new CriticalHit("Wrenched Arm", location, 2, "Your arm is almost pulled from its socket. Drop whatever is held in the associated hand; the arm is useless for 1d10 Rounds (see Amputated Parts).");
        else if(isBetween(secondRoll, 46, 50)) return new CriticalHit("Gaping Wound", location, 3, "The blow opens a deep, gaping wound. Gain 2 Bleeding Conditions. Until you receive Surgery to stitch up the cut, any associated Arm Damage you receive will also inflict 1 Bleeding Condition as the wound reopens.");
        else if(isBetween(secondRoll, 51, 55)) return new CriticalHit("Clean Brake", location, 3, "An audible crack resounds as the blow strikes your arm. Drop whatever was held in the associated hand and gain a Broken Bone (Minor) injury. Pass a Difficult (–10) Endurance Test or gain a Stunned Condition.");
        else if(isBetween(secondRoll, 56, 60)) return new CriticalHit("Raptured Ligament", location, 3, "You immediately drop whatever was held in that hand. Suffer a Torn Muscle (Major) injury.");
        else if(isBetween(secondRoll, 61, 65)) return new CriticalHit("Deep Cut", location, 3, "Gain 2 Bleeding Conditions as your arm is mangled. Gain 1 Stunned Condition and suffer a Torn Muscle (Minor) injury. Take a Hard (–20) Endurance Test or gain the Unconscious Condition. ");
        else if(isBetween(secondRoll, 66, 70)) return new CriticalHit("Damaged Artery", location, 4, "Gain 4 Bleeding Conditions. Until you receive Surgery, every time you take Damage to this Arm Hit Location gain 2 Bleeding Conditions.");
        else if(isBetween(secondRoll, 71, 75)) return new CriticalHit("Crushed Elbow", location, 4, "The blow crushes your elbow, splintering bone and cartilage. You immediately drop whatever was held in that hand and gain a Broken Bone (Major) injury.");
        else if(isBetween(secondRoll, 76, 80)) return new CriticalHit("Dislocated Shoulder", location, 4, "Your arm is wrenched out of its socket. Pass a Hard (–20) Endurance Test or gain the Stunned and Prone Condition. Drop whatever is held in that hand: the arm is useless and counts as lost (see Amputated Part). Gain 1 Stunned Condition until you receive Medical Attention. After this initial Medical Attention, an Extended Average (+20) Heal Test needing 6 SL is required to reset the arm, at which point you regain its use. Tests made using this arm suffer a –10 penalty for 1d10 days. ");
        else if(isBetween(secondRoll, 81, 85)) return new CriticalHit("Severed Finger", location, 4, "You gape in horror as a finger flies — Amputation (Average). Gain a Bleeding condition.");
        else if(isBetween(secondRoll, 86, 90)) return new CriticalHit("Cleft Hand", location, 5, "Your hand splays open from the blow. Lose 1 finger —Amputation (Difficult). Gain 2 Bleeding and 1 Stunned Condition.  For every succeeding Round in which you don't receive Medical Attention, you lose another finger as the wound tears; if you run out of fingers, you lose the hand — Amputation (Difficult).");
        else if(isBetween(secondRoll, 91, 93)) return new CriticalHit("Mauled Bicep", location, 5, "The blow almost separates bicep and tendon from bone, leaving an ugly wound that sprays blood over you and your opponent. You automatically drop anything held in the associated hand and suffers a Torn Muscle (Major) injury and 2 Bleeding and 1 Stunned Condition.");
        else if(isBetween(secondRoll, 94, 96)) return new CriticalHit("Mangled Hand", location, 5, "Your hand is left a mauled, bleeding mess. You lose your hand —Amputation (Hard). Gain 2 Bleeding Condition. Take a Hard (–20) Endurance Test or gain the Stunned and Prone Conditions. ");
        else if(isBetween(secondRoll, 97, 99)) return new CriticalHit("Sliced Tendons", location, 5, "Your tendons are cut by the blow, leaving your arm hanging useless — Amputation (Very Hard). Gain 3 Bleeding , 1 Prone , and 1 Stunned Condition. Pass a Hard (–20) Endurance Test or gain the Unconscious Condition.");
        else if(isBetween(secondRoll, 00, 00)) return new CriticalHit("Brutal Dismemberment", location, 99, "Your arm is severed, spraying arterial blood 1d10 feet in a random direction (see Scatter), before the blow follows through to your chest.");
        else                                               return new CriticalHit("error", "none", 0 ,"error in critical location");
    }

    public CriticalHit legCriticalWounds(Integer secondRoll, String location){
        if(isBetween(secondRoll, 1, 10))       return new CriticalHit("Stubbed Toe", location, 1, "In the scuffle, you stub your toe. Pass a Routine (+20) Endurance Test or suffer –10 on Agility Tests until the end of the next turn. ");
        else if(isBetween(secondRoll, 11, 20)) return new CriticalHit("Twisted Ankle", location, 1, "You go over your ankle, hurting it. Agility Tests suffer a –10  penalty for 1d10 rounds. ");
        else if(isBetween(secondRoll, 21, 25)) return new CriticalHit("Minor Cut", location, 1, "Gain 1 Bleeding Condition. ");
        else if(isBetween(secondRoll, 26, 30)) return new CriticalHit("Lost Footing", location, 1, "In the scuffle you lose your footing. Pass a Challenging (+0) Endurance Test or gain the Prone Condition. ");
        else if(isBetween(secondRoll, 31, 35)) return new CriticalHit("Thigh Strike", location, 2, "A painful blow slams into your upper thigh. Gain a Bleeding Condition and take an Average (+20) Endurance Test or stumble, gaining the Prone Condition.");
        else if(isBetween(secondRoll, 36, 40)) return new CriticalHit("Sprained Ankle", location, 2, "You sprain your ankle, giving you a Torn Muscle (Minor) injury.");
        else if(isBetween(secondRoll, 41, 45)) return new CriticalHit("Twisted Knee", location, 2, "You twist your knee too far. Agility Tests suffer a –20 penalty for 1d10 rounds.");
        else if(isBetween(secondRoll, 46, 50)) return new CriticalHit("Badly Cut Toe", location, 2, "Gain 1 Bleeding Condition. After the encounter, make a Challenging (+0) Endurance Test. If you fail, lose 1 toe —Amputation (Average).");
        else if(isBetween(secondRoll, 51, 55)) return new CriticalHit("Bad Cut", location, 3, "Gain 2 Bleeding conditions as a deep wound opens up your shin. Pass a Challenging (+0) Endurance Test or gain the Prone Condition.");
        else if(isBetween(secondRoll, 56, 60)) return new CriticalHit("Badly Twisted Knee", location, 3, "You badly twist your knee trying to avoid your opponent. Gain a Torn Muscle (Major) injury.");
        else if(isBetween(secondRoll, 61, 65)) return new CriticalHit("Hacked Leg", location, 3, "A cut bites down into the hip. Gain 1 Prone and 2 Bleeding Conditions, and suffer a Broken Bone (Minor) injury. Further, take a Hard (–20) Endurance Test or also gain a Stunned condition from the pain.");
        else if(isBetween(secondRoll, 66, 70)) return new CriticalHit("Torn Thigh", location, 3, "Gain 3 Bleeding Conditions as the weapon opens up your upper thigh. Pass a Challenging (+0) Endurance Test or gain the Prone Condition. Until you receive Surgery to stitch up the wound, each time you receive Damage to this Leg, also receive 1 Bleeding Condition. ");
        else if(isBetween(secondRoll, 71, 75)) return new CriticalHit("Ruptured Tendon", location, 4, "Gain a Prone and Stunned Condition as one of your tendons tears badly. Pass a Hard (–20) Endurance Test or gain the Unconscious Condition. Your leg is useless (see Amputated Parts). Suffer a Torn Muscle (Major) injury.");
        else if(isBetween(secondRoll, 76, 80)) return new CriticalHit("Carved Shin", location, 4, "The weapon drives clean through your leg by the knee, slicing into bone and through tendons. Gain a Stunned and Prone Condition. Further, suffer a Torn Muscle (Major) and Broken Bone (Minor) injury.");
        else if(isBetween(secondRoll, 81, 85)) return new CriticalHit("Broken Knee", location, 4, "The blow hacks into your kneecap, shattering it into several pieces. You gain 1 Bleeding , 1 Prone , and 1 Stunned Condition, and a Broken Bone (Major) Injury as you fall to the ground, clutching your ruined leg.");
        else if(isBetween(secondRoll, 86, 90)) return new CriticalHit("Dislocated Knee", location, 4, "Your knee is wrenched out of its socket. Gain the Prone Condition. Pass a Hard (–20) Endurance Test, or gain the Stunned Condition, which is not removed until you receive Medical Attention. After this initial Medical Attention, an Extended Average (+20) Heal Test needing 6 SL is required to reset the knee at which point you regain its use. Movement is halved, and Tests made using this leg suffer a –10 penalty for d10 days. ");
        else if(isBetween(secondRoll, 91, 93)) return new CriticalHit("Crushed Foot", location, 5, "The blow crushes your foot. Make an Average (+20) Endurance Test; if you fail, gain the Prone condition and lose 1 toe, plus 1 additional toe for each SL below 0 — Amputation (Average). Gain 2 Bleeding Conditions. If you don't receive Surgery within 1d10 days, you will lose the foot entirely.");
        else if(isBetween(secondRoll, 94, 96)) return new CriticalHit("Severed Foot", location, 5, "Your foot is severed at the ankle and lands 1d10 feet away in a random direction — Amputation (Hard) (see Scatter). You gain 3 Bleeding , 2 Stunned , and 1 Prone Condition.");
        else if(isBetween(secondRoll, 97, 99)) return new CriticalHit("Cut Tendon", location, 5, "A major tendon at the back of your leg is cut, causing you to scream out in pain as your leg collapses. Gain 2 Bleeding , 2 Stunned , and 1 Prone Condition and look on in horror as your leg never works again — Amputation (Very Hard).");
        else if(isBetween(secondRoll, 00, 00)) return new CriticalHit("Shattered Pelvis", location, 99, "The blow shatters your pelvis, severing one leg then driving through to the next. You die instantly from traumatic shock.");
        else                                               return new CriticalHit("error", "none", 0 ,"error in critical location");
    }

    public boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

}
