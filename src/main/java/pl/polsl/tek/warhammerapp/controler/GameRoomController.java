package pl.polsl.tek.warhammerapp.controler;

import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.polsl.tek.warhammerapp.dto.AttackMessageDto;
import pl.polsl.tek.warhammerapp.dto.BattleLogMessageDto;
import pl.polsl.tek.warhammerapp.dto.CombatMessageDto;
import pl.polsl.tek.warhammerapp.model.Character;
import pl.polsl.tek.warhammerapp.model.GameRoom;
import pl.polsl.tek.warhammerapp.model.presets.PresetItems;
import pl.polsl.tek.warhammerapp.services.GameRoomService;

import java.util.List;

@Controller
public class GameRoomController {

    private final GameRoomService gameRoomService;

    public GameRoomController(GameRoomService gameRoomService) {
        this.gameRoomService = gameRoomService;
    }
    // ------------------------------------ GET ------------------------------------------------------------------------
    @GetMapping("/roomList")
    @ResponseBody
    public List<String> getRooms() {
        return gameRoomService.getRoomList();
    }

    @GetMapping("/presetItems")
    @ResponseBody
    public PresetItems getPresetItems() {
        return PresetItems.getInstance();
    }



    @GetMapping("/playerList")
    @ResponseBody
    public List<String> getPlayers(@RequestParam String name) {
        return gameRoomService.getRoom(name).getConnectedPlayers();
    }

    @GetMapping("/room")
    @ResponseBody
    public GameRoom getRoom(@RequestParam String name) {
        return gameRoomService.getRoom(name);
    }

    @GetMapping("/hasGameMaster")
    @ResponseBody
    public Boolean getHasGameMaster(@RequestParam String name) {
        return gameRoomService.getRoom(name).getHasGameMaster();
    }
    //-------------------------------------- POST ----------------------------------------------------------------------

    @PostMapping("/newRoom")
    @ResponseStatus(value = HttpStatus.CREATED, reason = "Room created!")
    public void newGameRoom(@RequestBody GameRoom newGameRoom) {
        gameRoomService.addRoom(newGameRoom);
        System.out.println("Room " + newGameRoom.getName() + " was created");
    }

    @PostMapping("/newGamemaster")
    @ResponseStatus(value = HttpStatus.CREATED, reason = "Room created!")
    public void setNewGamemaster(@RequestParam String roomName, @RequestParam String gamemasterName) {
        if(gameRoomService.getRoomList().contains(roomName)){
            gameRoomService.getRoom(roomName).setHasGameMaster(true);
            gameRoomService.getRoom(roomName).setGameMaster(gamemasterName);
        }
        System.out.println("Gamemaster " + gamemasterName + " was added in room " + roomName);
    }


    @PostMapping("/newConnectedPlayer")
    @ResponseStatus(value = HttpStatus.CREATED, reason = "Room created!")
    public void newConnectedPlayerInRoom(@RequestParam String playerName, @RequestParam String roomName) {
        gameRoomService.getRoom(roomName).addConnectedPlayer(playerName);
        System.out.println("Player " + playerName + " was added in room " + roomName);
    }

    //-------------------------------------- DELETE --------------------------------------------------------------------

    @DeleteMapping("/closeRoom")
    @ResponseStatus(value = HttpStatus.OK, reason = "Room closed")
    public void closeRoom(@RequestParam String name) {
        if(gameRoomService.getRoomList().contains(name)) {
            gameRoomService.removeRoom(name);
            System.out.println("Room " + name + " was closed");
        } else {
            System.out.println("There was attempt to try and close room: " + name);
        }
    }

    @DeleteMapping("/removeConnectedPlayer")
    @ResponseStatus(value = HttpStatus.OK, reason = "Player remove")
    public void removePlayer(@RequestParam String playerName, @RequestParam String roomName) {
        GameRoom tmp = gameRoomService.getRoom(roomName);
        if(tmp.getConnectedPlayers().contains(playerName)) {
            gameRoomService.getRoom(roomName).removeConnectedPlayer(playerName);
            System.out.println("Player: " + playerName + " was remove from connected player list");
        } else {
            System.out.println("There was attempt to try and remove player " + playerName + " from room " + roomName);
        }
    }

    @DeleteMapping("/removeGamemaster")
    @ResponseStatus(value = HttpStatus.OK, reason = "Gamemaster status deleted")
    public void deleteGamemasterStatus(@RequestParam String roomName) {
        if(gameRoomService.getRoomList().contains(roomName)) {
            gameRoomService.getRoom(roomName).setHasGameMaster(false);
            gameRoomService.getRoom(roomName).setGameMaster("");
            System.out.println("Gamemaster leave room " + roomName);
        } else {
            System.out.println("There was attempt to try and remove gamemaster from room: " + roomName);
        }
    }
    //---------------------------------------- SOCKETS REQUEST ---------------------------------------------------------

    @MessageMapping("/generalroom/{roomName}/gameroom/sendbattlelog")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/battlelog")
    public String battleLogSocket(@DestinationVariable String roomName, @RequestBody BattleLogMessageDto message) {
        if (gameRoomService.getRoomList().contains(roomName)) {
            if(message.getMessage().charAt(0) == '#'){
                String answer = gameRoomService.getRoom(roomName).simpleTest(message.getMessage());
                if(answer.equals("")){
                    return gameRoomService.getRoom(roomName).addToBattlelog(message);
                } else {
                    return answer;
                }
            } else {
                return gameRoomService.getRoom(roomName).addToBattlelog(message);
            }
        }
        return null;
    }


    @MessageMapping("/generalroom/{roomName}/gameroom/sendplayerhit")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/playerhit")
    public CombatMessageDto characterAttackSocket(@DestinationVariable String roomName, @RequestBody AttackMessageDto message) {
        if (gameRoomService.getRoomList().contains(roomName)) {
            return gameRoomService.getRoom(roomName).attack(message);
        }
        return null;
    }

    @MessageMapping("/generalroom/{roomName}/gameroom/sendUpdatePlayer")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/updatePlayer")
    public Character playerUpdateSocket(@DestinationVariable String roomName, @RequestBody Character character) {
        if (gameRoomService.getRoomList().contains(roomName)) {
            GameRoom tmp = gameRoomService.getRoom(roomName);
            tmp.switchOrAddPlayer(character);
            System.out.println("podmiana postaci " + character.getCharName() + " w pokoju " +roomName);
            System.out.println(roomName);
            return character;
        }
        return null;
    }

    @MessageMapping("/generalroom/{roomName}/gameroom/sendUpdateEnemy")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/updateEnemy")
    public Character enemyUpdateSocket(@DestinationVariable String roomName, @RequestBody Character character) {
        if (gameRoomService.getRoomList().contains(roomName)) {
            GameRoom tmp = gameRoomService.getRoom(roomName);
            tmp.switchOrAddEnemy(character);
            System.out.println("podmiana postaci " + character.getCharName() + " w pokoju " +roomName);
            System.out.println(roomName);
            return character;
        }
        System.out.println("character update");
        System.out.println(roomName);
        return null;
    }

    @MessageMapping("/generalroom/{roomName}/gameroom/sendDeletePlayer")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/deletePlayer")
    public String playerDeleteSocket(@DestinationVariable String roomName, @RequestBody TextNode playerName) {
        String charName = playerName.asText();
        if (gameRoomService.getRoomList().contains(roomName)) {
            GameRoom tmp = gameRoomService.getRoom(roomName);
            Character tmpToDel = null;
            for (Character player : tmp.getPlayers()) {
                if (player.getCharName().equals(charName)) {
                    tmpToDel = player;
                    break;
                }
            }
            if(tmpToDel != null){ tmp.getPlayers().remove(tmpToDel);}
            System.out.println("delete player: " + charName + " from room: " + roomName);
            return charName;
        }
            return null;

    }
    @MessageMapping("/generalroom/{roomName}/gameroom/sendDeleteEnemy")
    @SendTo("/WarApp/generalroom/{roomName}/gameroom/deleteEnemy")
    public String enemyDeleteSocket(@DestinationVariable String roomName, @RequestBody TextNode enemyName) {
        String charName = enemyName.asText();
        if (gameRoomService.getRoomList().contains(roomName)) {
            GameRoom tmp = gameRoomService.getRoom(roomName);
            Character tmpToDel = null;
            for (Character enemy : tmp.getEnemies()) {
                if (enemy.getCharName().equals(charName)) {
                    tmpToDel = enemy;
                    break;
                }
            }
            if(tmpToDel != null){ tmp.getEnemies().remove(tmpToDel);}
            System.out.println("delete enemy: "  + charName + " from room: " + roomName);
            System.out.println(roomName);
            return charName;
        }
        return null;

    }
}
