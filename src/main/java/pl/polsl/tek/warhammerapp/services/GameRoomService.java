package pl.polsl.tek.warhammerapp.services;

import org.springframework.stereotype.Service;
import pl.polsl.tek.warhammerapp.model.GameRoom;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameRoomService {

    private List<GameRoom> allOfGameRooms;
    private List<String> listOfRooms;



    // --------------------------------- CONSTRUCTORS ------------------------------------------------------------------


    public GameRoomService() {
        this.allOfGameRooms = new ArrayList<GameRoom>();
        this.listOfRooms = new ArrayList<String>();
        GameRoom g1 = new GameRoom();
        g1.populateTestRoom();
        this.allOfGameRooms.add(g1);
        GameRoom g2 = new GameRoom();
        g2.populateOrphanRoom();
        this.allOfGameRooms.add(g2);
    }

    public GameRoomService(List<GameRoom> allOfGameRooms) {
        this.allOfGameRooms = allOfGameRooms;
        this.listOfRooms = new ArrayList<String>();
        for(GameRoom gameroom : allOfGameRooms){
            this.listOfRooms.add(gameroom.getName());
        }
    }

    // --------------------------------- BUISNESS LOGIC ----------------------------------------------------------------
    public void addRoom(GameRoom gameRoom){
        this.allOfGameRooms.add(gameRoom);
        this.listOfRooms.add(gameRoom.getName());
    }

    // --------------------------------- GETERS & SETTERS --------------------------------------------------------------
    public GameRoom getRoom(String gameroomName){
        for (GameRoom gameroom: allOfGameRooms) {
            if( gameroom.getName().equals(gameroomName)) {
                return gameroom;
            }
        }
        return null;
    }

    public void removeRoom(String gameroomName){
        GameRoom tmp = null;
        boolean found = false;
        for (GameRoom gameroom: allOfGameRooms) {
            if(gameroom.getName().equals(gameroomName)) {
                tmp = gameroom;
                found = true;
            }
        }
        if(found == true){
            this.listOfRooms.remove(tmp.getName());
            this.allOfGameRooms.remove(tmp);
        }
    }

    public List<String> getRoomList(){
        List<String> tmp = new ArrayList<>();
        for (GameRoom gameroom: allOfGameRooms) {
            tmp.add(gameroom.getName());
        }
        return tmp;
    }

//    public List<String> getRoomList() {
//        return listOfRooms;
//    }

    public void setListOfRooms(List<String> listOfRooms) {
        this.listOfRooms = listOfRooms;
    }

    public List<GameRoom> getAllOfGameRooms() {
        return allOfGameRooms;
    }

    public void setAllOfGameRooms(List<GameRoom> allOfGameRooms) {
        this.allOfGameRooms = allOfGameRooms;
    }

}
