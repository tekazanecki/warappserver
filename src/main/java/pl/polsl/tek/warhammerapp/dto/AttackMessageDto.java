package pl.polsl.tek.warhammerapp.dto;

public class AttackMessageDto {

    private String initializer;
    private String attackerName;
    private Integer attackerMod;
    private String defenderName;
    private Integer defenderMod;

    public AttackMessageDto() {
    }

    public AttackMessageDto(String initializer, String attackerName, Integer attackerMod, String defenderName, Integer defenderMod) {
        this.initializer = initializer; // 'player' or 'enemy'
        this.attackerName = attackerName;
        this.attackerMod = attackerMod;
        this.defenderName = defenderName;
        this.defenderMod = defenderMod;
    }

    public String getInitializer() {
        return initializer;
    }

    public void setInitializer(String initializer) {
        this.initializer = initializer;
    }

    public String getAttackerName() {
        return attackerName;
    }

    public void setAttackerName(String attackerName) {
        this.attackerName = attackerName;
    }

    public Integer getAttackerMod() {
        return attackerMod;
    }

    public void setAttackerMod(Integer attackerMod) {
        this.attackerMod = attackerMod;
    }

    public String getDefenderName() {
        return defenderName;
    }

    public void setDefenderName(String defenderName) {
        this.defenderName = defenderName;
    }

    public Integer getDefenderMod() {
        return defenderMod;
    }

    public void setDefenderMod(Integer defenderMod) {
        this.defenderMod = defenderMod;
    }
}
