package pl.polsl.tek.warhammerapp.dto;

import pl.polsl.tek.warhammerapp.model.CriticalHit;

import java.util.List;

public class CombatMessageDto {

    private String attackerCharName;
    private String attackerStatus;
    private Integer currentAttackerWounds;
    private List<CriticalHit> inAttackerCriticalHit;
    private String defenderCharName;
    private String defenderStatus;
    private Integer currentDefenderWounds;
    private List<CriticalHit> inDefenderCriticalHit;
    private String battleLog;


    public CombatMessageDto() {
    }

    public CombatMessageDto(String attackerCharName, String attackerStatus, Integer currentAttackerWounds, List<CriticalHit> inAttackerCriticalHit, String defenderCharName, String defenderStatus, Integer currentDefenderWounds, List<CriticalHit> inDefenderCriticalHit, String battleLog) {
        this.attackerCharName = attackerCharName;
        this.attackerStatus = attackerStatus;
        this.currentAttackerWounds = currentAttackerWounds;
        this.inAttackerCriticalHit = inAttackerCriticalHit;
        this.defenderCharName = defenderCharName;
        this.defenderStatus = defenderStatus;
        this.currentDefenderWounds = currentDefenderWounds;
        this.inDefenderCriticalHit = inDefenderCriticalHit;
        this.battleLog = battleLog;
    }

    public String getAttackerCharName() {
        return attackerCharName;
    }

    public void setAttackerCharName(String attackerCharName) {
        this.attackerCharName = attackerCharName;
    }

    public String getAttackerStatus() {
        return attackerStatus;
    }

    public void setAttackerStatus(String attackerStatus) {
        this.attackerStatus = attackerStatus;
    }

    public Integer getCurrentAttackerWounds() {
        return currentAttackerWounds;
    }

    public void setCurrentAttackerWounds(Integer currentAttackerWounds) {
        this.currentAttackerWounds = currentAttackerWounds;
    }

    public List<CriticalHit> getInAttackerCriticalHit() {
        return inAttackerCriticalHit;
    }

    public void setInAttackerCriticalHit(List<CriticalHit> inAttackerCriticalHit) {
        this.inAttackerCriticalHit = inAttackerCriticalHit;
    }

    public String getDefenderCharName() {
        return defenderCharName;
    }

    public void setDefenderCharName(String defenderCharName) {
        this.defenderCharName = defenderCharName;
    }

    public String getDefenderStatus() {
        return defenderStatus;
    }

    public void setDefenderStatus(String defenderStatus) {
        this.defenderStatus = defenderStatus;
    }

    public Integer getCurrentDefenderWounds() {
        return currentDefenderWounds;
    }

    public void setCurrentDefenderWounds(Integer currentDefenderWounds) {
        this.currentDefenderWounds = currentDefenderWounds;
    }

    public List<CriticalHit> getInDefenderCriticalHit() {
        return inDefenderCriticalHit;
    }

    public void setInDefenderCriticalHit(List<CriticalHit> inDefenderCriticalHit) {
        this.inDefenderCriticalHit = inDefenderCriticalHit;
    }

    public String getBattleLog() {
        return battleLog;
    }

    public void setBattleLog(String battleLog) {
        this.battleLog = battleLog;
    }
}
