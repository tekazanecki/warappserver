package pl.polsl.tek.warhammerapp.dto;

public class BattleLogMessageDto {

    private String sender;
    private String status;
    private String message;

    public BattleLogMessageDto() {
    }

    public BattleLogMessageDto(String message, String sender, String status) {
        this.message = message;
        this.sender = sender;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
